.PHONY: build
build:
	cd Dockerfiles && ./build

.PHONY: box86
box86:
	IMG=box86 ./toybox_86_64

.PHONY: wine32
wine32:
        IMG=box86.wine ./toybox_86_64

.PHONY: steamcmd32
steamcmd:
        IMG=box86.steamcmd ./toybox_86_64

.PHONY: box64
box64:
        IMG=box64 ./toybox_86_64

.PHONY: wine64
wine64:
	IMG=box64.wine ./toybox_86_64

.PHONY: steamcmd64
steamcmd64:
	IMG=box64.steamcmd ./toybox_86_64

.PHONY: chromium-widevine
chromium-widevine:
	./toybox_86_64 hthiemann/docker-chromium-armhf

