#!/bin/bash
set -e

apps() {
	echo -e "\nBuilding docker image "${IMG##*/}" using tag "${IMG##*/Dockerfile.}"\n"
	docker buildx build \
		--platform linux/arm64 \
		--push \
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
		--build-arg BASE=${BASE} \
		--build-arg BSP_VER=${BSP_VER} \
		--build-arg SOC=${SOC} \
		-t "${CI_REGISTRY_IMAGE}:l4t-${IMG##*/Dockerfile.}-${BASE}" \
		-f "${IMG}" \
		"${IMG%%/*}"
}

build() {
	#
	# Build base container
	#

	cd base

	docker buildx build \
		--platform linux/arm64 \
		--push \
		--build-arg BASE=${BASE} \
		--build-arg L4S_BSP=${L4S_BSP} \
		--build-arg BSP_VER=${BSP_VER} \
		--build-arg BSP=${BSP} \
		--build-arg SOC=${SOC} \
		--build-arg CHIP_ID=${CHIP_ID} \
		-t "${CI_REGISTRY_IMAGE}:l4t-base-${SOC}-r${BSP_VER}-${BASE}" \
		-f Dockerfile.base .

	docker buildx build \
		--platform linux/arm64 \
		--push \
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
		--build-arg BASE=${BASE} \
		--build-arg BSP_VER=${BSP_VER} \
		--build-arg SOC=${SOC} \
		-t "${CI_REGISTRY_IMAGE}:l4t-cuda-$SOC-r${BSP_VER}-${BASE}" \
		-f Dockerfile.cuda .
	
	if [[ "${BASE}" = "bionic" ]]; then
		docker buildx build \
			--platform linux/arm64 \
			--push \
			--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
			--build-arg BASE=${BASE} \
			--build-arg BSP_VER=${BSP_VER} \
			--build-arg SOC=${SOC} \
			-t "${CI_REGISTRY_IMAGE}:l4t-ffmpeg-$SOC-r${BSP_VER}-${BASE}" \
			-f Dockerfile.ffmpeg .
	fi

	cd ..

	for IMG in $(find -name "Dockerfile.*" -not -path "./base/*" -not -path "./applications/box*/*" -not -path "./virt/*"); do
		if [[ "${BASE}" != "jammy" && "${IMG}" =~ "FEX" ]]; then
			echo "Skipping ${IMG} for ${BASE}"
		elif [[ "${BASE}" != "bionic" && "${IMG}" =~ "firefox" ]]; then
			echo "Skipping ${IMG} for ${BASE}"
		else
			apps
		fi
	done
}

#
# Build base container
#

# Build t210 32.3.1
export BSP_VER=32
export BSP=https://developer.nvidia.com/embedded/dlc/r32-3-1_Release_v1.0/t210ref_release_aarch64/Tegra210_Linux_R32.3.1_aarch64.tbz2
export SOC=t210
export JOB=bsp-tx1
export L4S_BSP=https://gitlab.com/l4t-community/gnu-linux/switchroot-pipeline/-/jobs/artifacts/master/raw/l4s-bsp-tx1-32.3.1/l4s-bsp-32.3.1.7z?job=$JOB
export CHIP_ID="0x21"

build

# Build t210 32.7.1
export BSP_VER=32.7
export BSP=https://developer.nvidia.com/embedded/l4t/r32_release_v7.1/t210/jetson-210_linux_r32.7.1_aarch64.tbz2
export SOC=t210
export JOB=bsp-tx1-32.7.1
export L4S_BSP=https://gitlab.com/l4t-community/gnu-linux/switchroot-pipeline/-/jobs/artifacts/master/raw/l4s-bsp-tx1-$BSP_VER.1/l4s-bsp-$BSP_VER.1.7z?job=$JOB
export CHIP_ID="0x21"

build

# Build t194 32.7.1
export BSP_VER=32.7
export BSP=https://developer.nvidia.com/embedded/l4t/r32_release_v7.1/t186/jetson_linux_r32.7.1_aarch64.tbz2
export SOC=t194
export JOB=bsp-xavier-32.7.1
export L4S_BSP=https://gitlab.com/l4t-community/gnu-linux/switchroot-pipeline/-/jobs/artifacts/master/raw/l4s-bsp-xavier-$BSP_VER.1/l4s-bsp-$BSP_VER.1.7z?job=$JOB
export CHIP_ID="0x19"

build

echo -e "Build all successfully\n"
